# nixos-webserver

## Replace Debian OS with NixOS 

### Requirements

- `curl` (`$ sudo apt install curl`)
- editor (e.g. `$ sudo apt install vim`)

### NIXOS_LUSTRATE - install in place of an existing non-NixOS Linux distribution

- Official manual: https://nixos.org/manual/nixos/stable/#sec-installing-from-other-distro

#### Add non-root user

As installing Nix as root is not supported this way. The following is taken from https://github.com/aveltras/deploying-your-app-with-nixos?tab=readme-ov-file#the-nixos-lustrate-method:

```sh
$ adduser {username}           # create user
$ usermod -aG sudo {username}  # add user to 'sudo' group
$ su - {username}              # change to the new user
```

#### Install Nix package manager

See [official manual](https://nixos.org/manual/nixos/stable/#sec-installing-from-other-distro)

```sh
$ curl -L https://nixos.org/nix/install | sh  # download and install
$ . $HOME/.nix-profile/etc/profile.d/nix.sh   # source nix cli (alternative: open new shell)
```

##### Change channel (optional)

By default the unstable channel will be used. Most of the time this should be no problem. If a stable channel is wanted, the channel can be changed by running

```sh
$ nix-channel --add https://nixos.org/channels/nixos-version nixpkgs
$ nix-channel --update
```

#### Install installation tools

```sh
$ nix-env -f '<nixpkgs>' -iA nixos-install-tools
```

#### Generate NixOS configuration

```sh
$ sudo `which nixos-generate-config`
```

There may be an warning about a failed locale. This can probably be ignored.

#### Update NixOS configuration

To make sure the server is still accessible after the rebuild and reboot, the following changes should be made to the `/etc/nixos/configuration.nix`:

```nix
# /etc/nixos/configuration.nix
{ config, lib, pkgs, ... }:
{
    # ...

+   boot.loader.grub.device = "nodev";

+   services.openssh.enable = true;

+   users.users.root.openssh.authorizedKeys.keys = [
+     "{public ssh key as string}"
+   ];

    # ...
}

```

#### Build the NixOS closure and install it in the `system` profile

```sh
$ nix-env -p /nix/var/nix/profiles/system -f '<nixpkgs/nixos>' -I nixos-config=/etc/nixos/configuration.nix -iA system
```

#### Change ownership of the `/nix` tree to root

```sh
$ sudo chown -R 0:0 /nix
```

#### Setup NIXOS and NIXOS_LUSTRATE files

```sh
$ sudo touch /etc/NIXOS
$ sudo touch /etc/NIXOS_LUSTRATE
$ echo etc/nixos | sudo tee -a /etc/NIXOS_LUSTRATE  # Makes sure that the NixOS configuration files are kept on reboot
```

#### Move `/boot` directory

Warning: Current distribution will no longer be bootable after this step.

```sh
$ sudo mv -v /boot /boot.bak  # or if this does not work: `sudo cp -a -v /boot /boot.bak`
$ sudo /nix/var/nix/profiles/system/bin/switch-to-configuration boot
```

#### Reboot

```sh
$ exit  # return to root user
$ reboot
```

#### Cleanup

```sh
$ rm -rf /old-root                # delete old debian files / optional / did not work for me - some errors with permissions
$ nix-channel --update            # update NixOS channels
$ nixos-rebuild switch --upgrade  # update NixOS
$ nix-collect-garbage -d          # clean the nix store
```

#### Alternatives

- https://github.com/elitak/nixos-infect